class Person{
    public constructor(private readonly name:string){

    }
    public getName():string{
        return this.name;
    }
}

const person = new Person("jane");
console.log(person);
console.log(person.getName());

interface Shape{
    getArea:()=> number;
}
class Rectangle1 implements Shape{
    public constructor(protected readonly width :number,protected readonly hright:number){}
    public getArea():number{
        return this.width*this.hright;
    }


}
const rect:Rectangle1 = new Rectangle1(50,10);
console.log(rect);
console.log(rect.getArea())

class Square extends Rectangle1{
    public constructor(width:number){
        super(width,width);
    }
    public toString():string{
        return `Square[${this.width}]`;
    }
}
const sq= new Square(10);
console.log(sq);
console.log(sq.getArea());
console.log(sq.toString());

abstract class polygon{
    public abstract getArea():number;

    public toString():string{
        return `Polygon[area=${this.getArea}]`
    }
}
class Rectangle2 extends polygon{
    public constructor(protected readonly width :number,protected readonly height:number){
        super();
    }
    public getArea(): number {
        throw new Error("Method not implemented");
    }
}
const rect2:Rectangle2= new Rectangle2 = new Rectangle2(50,10);
console.log(rect2);
console.log(rect2.toString())